package beans;

import java.io.Serializable;



	//Userテーブルのbeans

public class FavoriteBeans implements Serializable {
		private int id;
		private int userId;
		private int itemId;



		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}
		public int getItemId() {
			return itemId;
		}
		public void setItemId(int itemId) {
			this.itemId = itemId;
		}

}