<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>商品購入確認</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/518400799d.js"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/item.css">

</head>
<body>
	<header>
		<p>SELECT STORE</p>
		<nav>
			<ul>
				<li><a href="index.html"><i class="fa fa-home"> HOME</i></a></li>
				<li><a href="user_info.html"><i class="fas fa-user"></i> マイページ</a></li>
				<li><a href="item_curt.html"><i class="fas fa-shopping-cart"></i> カート</a></li>
				<li><a href="item_favorite.html"><i class="far fa-heart"></i> お気に入り</a></li>
				<li><a href="item_inquiry.html"><i class="far fa-paper-plane"></i> お問い合わせ</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<br>
  		<div style="text-align:center">
				<h1><b>商品購入確認</b></h1>
					<div id="line"></div>
		</div>
		<div id="submenu">
			<div id="category_list">
  			</div>
		</div>

		 <!-- 購入フローアイコン -->
		<div id="buy_flow">
   					<i class="fas fa-shopping-cart fa-5x"></i>
   						<div id="flow_number">
       						 <p><b>1</b></p>
       					</div>
       					<div id="flow_text">
       					 	<p><b>商品購入詳細</b></p>
       					 </div>
       	</div>
       		<div id="buy_flow">
       			<div id="flow_sign">
       				 <p><b>></b></p>
       			</div>
       	</div>
       	<div id="buy_flow">
       			<i class="far fa-check-square fa-5x"></i>
       	<div id="flow_number">
       						 <p><b>2</b></p>
       					</div>
       					<div id="flow_text">
       					 	<p><b>購入内容確認</b></p>
       					 </div>
       	</div>
       		<div id="buy_flow">
       			<div id="flow_sign">
       				 <p><b>></b></p>
       			</div>
       	</div>
       	<div id="buy_flow">
       			<i class="fas fa-truck fa-5x"></i>
       	<div id="flow_number">
       						 <p><b>3</b></p>
       					</div>
       					<div id="flow_text">
       					 	<p><b>商品購入完了</b></p>
       					 </div>
       	</div>

		<!-- 購入テーブル -->
		<div id="item_table">
			<table class="table">
 				 <thead class="thead-dark">
  					  <tr align="center">
   						  <th scope="col">商品番号</th>
    					  <th scope="col">商品名</th>
    					  <th scope="col">サイズ</th>
    					  <th scope="col">個数</th>
   					   	  <th scope="col">金額</th>
 					  </tr>
 				 </thead>
 				 <tbody>
   					   <tr align="center">
   						   	<td scope="row">1</td>
    						 	 <td>CREW NECK SWEATSHIRT</td>
    						 	 <td>M</td>
      						 	 <td>1</td>
    						 	 <td>¥15120</td>
  					   </tr>
   						<tr align="center">
    						  <td scope="row">2</td>
     								<td>THE ORIGINAL TWIN TIPPED FRED PERRY SHIRT</td>
     								<td>M</td>
						     	 	<td>1</td>
						    	    <td>¥12,960</td>
						</tr>
						<tr align="center">
						      <td scope="row">3</td>
						      		<td>LAYERED SHIRT</td>
						      		<td>M</td>
						      		<td>1</td>
						     		<td>¥16,200</td>
						</tr>
						<thead class="thead-light">
  					  <tr align="center">
  					  	  <th scope="col"></th>
    					  <th scope="col"></th>
   						  <th scope="col"></th>
    					  <th scope="col"></th>
    					  <th scope="col">
    					  	<div class="form-group">
    					  		<div id="table_select">
    					  		<label class="my-1 mr-2" for="inlineFormCustomSelectPref"><b>配送方法</b></label>
  					 				<div style= width:200px;>
 										 <select class="custom-select my-1 mr-sm-2" name="size" id="inlineFormCustomSelectPref">
   												<option selected>-------</option>
  												<option value="1">通常配送 (無料)</option>
    											<option value="2">速達配送 (¥500)</option>
   												<option value="3">日時指定配送 (¥100)</option>
 								 </select>
 								 </div>
 								</div>
 							</div>
 							</th>

 					  </tr>
 				 </thead>
			</table>
		</div>
			<div id="line"></div>
			<div style="text-align:center;">
					<button type="button" class="btn btn-primary btn-lg">変更 <i class="fas fa-shopping-cart"></i> </button>
				<div id="buy_button">
					<button type="button" class="btn btn-secondary btn-lg">次へ</button>
				</div>
			</div>
</body>
</html>