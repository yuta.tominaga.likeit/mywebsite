<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー情報</title>
	<!-- Boostrapにアクセス-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/518400799d.js"></script>
	<link rel="stylesheet" href="css/login.css">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/item.css">

</head>
	<body>
		<header>
		<p>SELECT STORE</p>
		<nav>
			<ul>
				<li><a href="index.html"><i class="fa fa-home"> HOME</i></a></li>
				<li><a href="item_curt.html"><i class="fas fa-shopping-cart"></i> カート</a></li>
				<li><a href="item_favorite.html"><i class="far fa-heart"></i> お気に入り</a></li>
				<li><a href="logout.html"><i class="fas fa-key"></i> ログアウト</a></li>
				<li><a href="item_inquiry.html"><i class="far fa-paper-plane"></i> お問い合わせ</a></li>
				<!-- 管理者でログインした場合のみ表示 -->
				<li><a href="admin_index.html"><i class="fas fa-wrench"></i> 管理画面</a></li>

			</ul>
		</nav>
	</header>
	<br>
	<br>
			<div style="text-align:center">
				<h1><b>ユーザー情報</b></h1>
			<div id="line"></div>
				<table class="table2" align="center">
				  <tbody>
				    <tr>
				      <th scope="row">ログインID</th>
				      <td>id0001</td>
				    </tr>
				     <tr>
				      <th scope="row">ユーザー名: </th>
				      <td>田中太郎</td>
				    </tr>
				     <tr>
				      <th scope="row">名前</th>
				      <td>田中太郎</td>
				    </tr>
				     <tr>
				      <th scope="row">生年月日 </th>
				      <td>1989年04月26日</td>
				    </tr>
				     <tr>
				      <th scope="row">年齢</th>
				      <td>２９歳</td>
				    </tr>
				     <tr>
				      <th scope="row">住所</th>
				      <td>東京都中野区</td>
				    </tr>
				  </tbody>
				</table>

		<br>
			<button type="button" class="btn btn-primary btn-lg">変更</button>
		</div>
		<br>
		<br>
			<table class="table1" align="center">
 			   <tr>
    				  <th scope="col"></th>
				      <th scope="col">購入日時</th>
				      <th scope="col">配送方法</th>
				      <th scope="col">金額</th>
				    </tr>
				  <tbody>
				    <tr>
				      <td scope="row"><a href="user_buy_detail.html"><i class="far fa-caret-square-down fa-2x"></i></a></td>
				      <td>2019.09.10</td>
				      <td>日時指定配送</td>
				      <td>15,220円</td>
				    </tr>
				    <tr>
				      <td scope="row"><a href="user_buy_detail.html"><i class="far fa-caret-square-down fa-2x"></i></a></td>
				      <td>2019.09.10</td>
				      <td>普通配送</td>
				      <td>10298739円</td>
				    </tr>
				    <tr>
				      <td scope="row"><a href="user_buy_detail.html"><i class="far fa-caret-square-down fa-2x"></i></a></td>
				      <td>2019.09.10</td>
				      <td>速達便</td>
				      <td>98376円</td>
				    </tr>
				  </tbody>
				</table>
			<br>
			<br>
		</div>

	</body>
</html>