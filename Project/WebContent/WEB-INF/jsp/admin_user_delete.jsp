<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー情報削除</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/518400799d.js"></script>
	<link rel="stylesheet" href="css/login.css">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/item.css">

</head>
<body>
	<header>
		<p>SELECT STORE</p>
		<nav>
			<ul>
					<li><a href="admin_index.html"><i class="fa fa-home"></i> 管理者画面</a></li>
					<li><a href="admin_user_list.html"><i class="fas fa-user"></i> ユーザーリスト</a></li>
					<li><a href="admin_item_request_list.html"><i class="far fa-heart"></i> 商品リクエスト</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<br>
  		<div style="text-align:center">
				<h1><b>ユーザー情報削除</b></h1>
					<div id="line"></div>
	<br>
				<font size="6">ログインID：${user.loginId}</font>
				<font size="4">を本当に削除してよろしいでしょうか。</font>
	<br>
	<br>
				<input type="button"value="キャンセル"onclick="location.href='userListServlet'">
	<br>
	<br>
				<form class="form-signin" action="UserDeleteServlet" method="post">
					<input type="hidden" name="id" value="${user.id}">
					<input type="submit" name="OK" value="OK">
				</form>
	<br>
	<br>
		</div>
</body>
</html>