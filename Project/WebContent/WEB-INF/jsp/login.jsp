<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>ログイン</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/518400799d.js"></script>
	<link rel="stylesheet" href="css/login.css">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/item.css">

</head>
<body>
	<header>
		<p>SELECT STORE</p>
		<nav>
			<ul>
				<li><a href="Index"><i class="fa fa-home"> HOME</i></a></li>
				<li><a href="UserRegistServlet"><i class="fas fa-edit"></i> 新規登録</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<br>
	<br>
	<div class="new-form">
	<div style="text-align:center">
<br>
			<h1><b>ログイン</b></h1>
	</div>
<br>
	<div class="centering_text">
			<h2><font size="4">ID・パスワードをご入力ください</font></h2>
	</div>
			<div class="centering_item">
				<label for="exampleInputEmail1">ログインID</label>
			<div style= width:400px;>
				<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="ログインID">
    		</div>
    		</div>
    	<br>
    	<div class="centering_item">
    		<div style= width:400px;>
				<label for="exampleInputEmail1">パスワード</label>
    			<input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="パスワード">
			</div>
			</div>
		<br>
			<div style="text-align:center">
				<p>	<button type="submit" class="btn btn-primary">ログイン</button></p>

				<p> <a href="UserRegistServlet">アカウントの作成はコチラ</a></p>
			</div>
	</div>
	</body>
</html>