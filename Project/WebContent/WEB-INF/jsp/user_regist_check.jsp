<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ登録情報確認画面</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/518400799d.js"></script>
	<link rel="stylesheet" href="css/login.css">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/item.css">

</head>
<body>
	<header>
		<p>SELECT STORE</p>
		<nav>
			<ul>
				<li><a href="index.html"><i class="fa fa-home"> HOME</i></a></li>
				<li><a href="item_inquiry.html"><i class="far fa-paper-plane"></i> お問い合わせ</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<br>
		<div style="text-align:center">
			<p><font size="6"><b>登録確認</b></font></p>
			<div id="line"></div>
		<br>
		<div class="new-form">
		<br>
  						<p><font size="4"><b>ログインID : admin</b></font></p>
   		<br>
 						<p><font size="4"><b>ユーザー名 : 管理者</b></font></p>
 		<br>
  						<p><font size="4"><b>名前 : 名前</b></font></p>
  		<br>
  						<p><font size="4"><b>住所 : 東京都</b></font></p>
  		<br>
  						<p><font size="4"><b>パスワード : password</b></font></p>
  		<br>
  					<div style="text-align:center">
  							<button type="submit" class="btn btn-primary my-1">登録</button>
  					</div>
  		<br>
			</div>
		</div>
	</body>
</html>