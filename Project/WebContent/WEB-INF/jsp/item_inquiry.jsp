<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>お問い合わせフォーム</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/518400799d.js"></script>
	<link rel="stylesheet" href="css/login.css">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/item.css">

</head>
<body>
	<header>
		<p>SELECT STORE</p>
		<nav>
			<ul>
				<li><a href="index.html"><i class="fa fa-home"> HOME</i></a></li>
				<li><a href="user_regist.html"><i class="fas fa-edit"></i> 新規登録</a></li>
				<li><a href="login.html"><i class="fas fa-key"></i> ログイン</a></li>
			<!-- 下4つログインしている場合のみ表示 -->
				<li><a href="user_info.html"><i class="fas fa-user"></i> マイページ</a></li>
				<li><a href="item_curt.html"><i class="fas fa-shopping-cart"></i> カート</a></li>
				<li><a href="item_favorite.html"><i class="far fa-heart"></i> お気に入り</a></li>
				<li><a href="logout.html"><i class="fas fa-key"></i> ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<br>
  		<div style="text-align:center">
				<h1><b>お問い合わせフォーム</b></h1>
					<div id="line"></div>
		</div>
			<br>
			<div class="new-form">
				<form>
  					<div class="form-group">
  						<div style= width:400px;>
    						<label for="formGroupExampleInput2"><b>ユーザー名</b></label>
   							 <input type="text" class="form-control" name="user_name" id="formGroupExampleInput2" placeholder="User name">
  						</div>
  					</div>
  						<label for="formGroupExampleInput2"><b>名前</b></label>
  					<div class="form-row" style= width:600px;>
  						 <div class="col">
      							<b>姓</b><input type="text" name="last_name" class="form-control" placeholder="Last name">
  						 </div>
   						 <div class="col">
   							  	<b>名</b><input type="text"  name="first_name" class="form-control" placeholder="First name">
  						 </div>
  					</div>
  				<br>
    				 <div class="form-group">
    					<label for="formGroupExampleInput2"><b>メールアドレス</b></label>
    						<div style= width:500px;>
   								<input type="email" class="form-control" name="mail_adress" id="formGroupExampleInput2" placeholder="mail adress">
  							</div>
  					 </div>
  				<br>
  					 <div class="form-group">
  						<div style= width:400px;>
    						<label for="formGroupExampleInput2"><b>件名</b></label>
   							 <input type="text" class="form-control" name="ininquiry_title" id="formGroupExampleInput2" placeholder="title">
  						</div>
  					</div>
  				<br>
 					 <div class="form-group">
   						 <label for="exampleFormControlTextarea1"><b>お問い合わせ内容</b></label>
   						 <textarea class="form-control" id="exampleFormControlTextarea1" rows="8"></textarea>
 					 </div>
  				<br>
  					<div style="text-align:center">
  							<button type="submit" class="btn btn-primary my-1">送信</button>
  				<br>
  				<br>
  					<a href="index.html">戻る</a>
  					</div>
				</form>
			</div>
	</body>
</html>