<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>カート</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/518400799d.js"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/item.css">

</head>
<body>
	<header>
		<p>SELECT STORE</p>
		<nav>
			<ul>
				<li><a href="index.html"><i class="fa fa-home"> HOME</i></a></li>
				<li><a href="user_regist.html"><i class="fas fa-edit"></i> 新規登録</a></li>
				<li><a href="login.html"><i class="fas fa-key"></i> ログイン</a></li>
			<!-- 下3つログインしている場合のみ表示 -->
				<li><a href="user_info.html"><i class="fas fa-user"></i> マイページ</a></li>
				<li><a href="item_favorite.html"><i class="far fa-heart"></i> お気に入り</a></li>
				<li><a href="logout.html"><i class="fas fa-key"></i> ログアウト</a></li>
				<li><a href="item_inquiry.html"><i class="far fa-paper-plane"></i> お問い合わせ</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<br>
  		<div style="text-align:center">
				<h1><b>ショッピングカート</b></h1>
					<div id="line"></div>
		</div>

		<div id="submenu">
			<ul class = "category">
  				<li><a href="index.html"><i class="fa fa-home"> HOME</i> </a></li>
  				<li>＜ </li>
  				<li> <i class="fas fa-shopping-cart"></i>  ショッピングカート</li>
  			</ul>
		</div>

			<table class="table">
 				 <thead class="thead-dark">
  					  <tr align="center">
   						  <th scope="col">商品番号</th>
    					  <th scope="col">商品名</th>
    					  <th scope="col">個数</th>
   					   	  <th scope="col">金額</th>
 					  </tr>
 				 </thead>
 				 <tbody>
   					   <tr align="center">
   						   	<th scope="row">1</th>
    						 	 <td>CREW NECK SWEATSHIRT</td>
      						 	 <td>1</td>
    						 	 <td>¥15120</td>
  					   </tr>
   						<tr align="center">
    						  <th scope="row">2</th>
     								<td>THE ORIGINAL TWIN TIPPED FRED PERRY SHIRT</td>
						     	 	<td>1</td>
						    	    <td>¥12,960</td>
						</tr>
						<tr align="center">
						      <th scope="row">3</th>
						      		<td>LAYERED SHIRT</td>
						      		<td>1</td>
						     		<td>¥16,200</td>
						</tr>
						<thead class="thead-light">
  					  <tr align="center">
  					  	  <th scope="col"></th>
    					  <th scope="col"></th>
   						  <th scope="col">総数</th>
    					  <th scope="col">合計金額</th>

 					  </tr>
 				 </thead>
 				 <tbody>
   					   <tr align="center">
   						   	<th scope="row"></th>
    						 	 <td></td>
      						 	 <td>3</td>
    						 	 <td>¥44280(税込)</td>
  					   </tr>
				</tbody>
			</table>
			通貨：円
			<div id="line"></div>
			<div style="text-align:center;">
					<button type="button" class="btn btn-primary btn-lg">買い物を続ける <i class="fas fa-shopping-cart"></i> </button>
					<button type="button" class="btn btn-secondary btn-lg">購入画面へ</button>
			</div>
	<br>
	<br>




</body>
</html>