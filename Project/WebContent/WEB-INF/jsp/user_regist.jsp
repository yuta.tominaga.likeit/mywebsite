<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>新規登録</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/518400799d.js"></script>
	<link rel="stylesheet" href="css/login.css">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/item.css">

</head>
<body>
	<header>
		<p>SELECT STORE</p>
		<nav>
			<ul>
				<li><a href="index.html"><i class="fa fa-home"> HOME</i></a></li>
				<li><a href="login.html"><i class="fas fa-key"></i> ログイン</a></li>
				<li><a href="item_inquiry.html"><i class="far fa-paper-plane"></i> お問い合わせ</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<br>
		<div style="text-align:center">
			<h1><b>新規登録</b></h1>
			<div id="line"></div>
		</div>
	<br>
			<div class="buy-form">
				<form>
  					<div class="form-group">
  						<div style= width:400px;>
   					 	<label for="formGroupExampleInput"><b>ログインID</b></label>
    					<input type="text" class="form-control" name="login_id" id="formGroupExampleInput" placeholder="Login ID">
    		<br>
    					<label for="formGroupExampleInput2"><b>ユーザー名</b></label>
   						 <input type="text" class="form-control" name="user_name" id="formGroupExampleInput2" placeholder="User name">
  						</div>
  					</div>
  						<label for="formGroupExampleInput2"><b>名前</b></label>
  					<div class="form-row" style= width:600px;>
  						 <div class="col">
      							<b>姓</b><input type="text" name="last_name" class="form-control" placeholder="Last name">
  						 </div>
   						 <div class="col">
   							  	<b>名</b><input type="text"  name="first_name" class="form-control" placeholder="First name">
  						 </div>
  					</div>
  				<br>
  					 <div class="form-group">
    					<label for="formGroupExampleInput2"><b>生年月日</b></label>
    			<br>
   						<input type="date" name="calendar" max="9999-12-31">
   					 </div>
    				 <div class="form-group">
    					<label for="formGroupExampleInput2"><b>年齢</b></label>
    						<div style= width:200px;>
   								<input type="text" class="form-control" name="user_age" id="formGroupExampleInput2" placeholder="age">
  							</div>
  					 </div>
 					 <div class="form-group">
  						<label class="my-1 mr-2" for="inlineFormCustomSelectPref"><b>住所</b></label>
  					 		<div style= width:200px;>
 								 <select class="custom-select my-1 mr-sm-2" name="adress1" id="inlineFormCustomSelectPref">
   										<option selected>-------</option>
  											<option value="1">北海道</option>
    										<option value="2">青森</option>
   											<option value="3">岩手</option>
   											<option value="4">宮城</option>
    										<option value="5">秋田</option>
    										<option value="6">山形</option>
    										<option value="7">福島</option>
   											<option value="8">茨城</option>
   											<option value="9">栃木</option>
    										<option value="10">群馬</option>
    										<option value="11">埼玉</option>
    										<option value="12">千葉</option>
   											<option value="13">東京</option>
   											<option value="14">神奈川</option>
    										<option value="15">新潟</option>
    										<option value="16">富山</option>
    										<option value="17">石川</option>
   											<option value="18">福井</option>
   											<option value="19">山梨</option>
    										<option value="20">長野</option>
    										<option value="21">岐阜</option>
    										<option value="22">静岡</option>
   											<option value="23">愛知</option>
   											<option value="24">三重</option>
    										<option value="25">滋賀</option>
    										<option value="26">京都</option>
    										<option value="27">大坂</option>
   											<option value="28">兵庫</option>
   											<option value="29">奈良</option>
    										<option value="30">和歌山</option>
    										<option value="31">鳥取</option>
    										<option value="32">島根</option>
   											<option value="33">岡山</option>
   											<option value="34">広島</option>
    										<option value="35">山口</option>
    										<option value="36">徳島</option>
    										<option value="37">香川</option>
   											<option value="38">愛媛</option>
   											<option value="39">高知</option>
    										<option value="40">福岡</option>
    										<option value="41">佐賀</option>
    										<option value="42">長崎</option>
   											<option value="43">熊本</option>
   											<option value="44">大分</option>
    										<option value="45">宮崎</option>
    										<option value="46">鹿児島</option>
    										<option value="47">沖縄</option>
 								 </select>
 							</div>
 								<div style= width:700px;>
   									<input type="text" class="form-control" name="adress2" id="formGroupExampleInput2" placeholder="adress">
   								</div>
   						<br>
   								<div style= width:200px;>
   					 				<label for="formGroupExampleInput"><b>パスワード</b></label>
    								<input type="password" class="form-control" name="password" id="inputPassword5" placeholder="password">
    					<br>
    								<label for="formGroupExampleInput2"><b>パスワード(確認)</b></label>
   									<input type="password" class="form-control" name="pass_check" id="inputPassword5" placeholder="password(check)">
   								</div>
		  			</div>
  				<br>
  					<div style="text-align:center">
  							<button type="submit" class="btn btn-primary my-1">新規登録</button>
  				<br>
  				<br>
  					<a href="index.html">戻る</a>
  					</div>
				</form>
			</div>
		<br>
		<br>
	</body>
</html>