<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>リクエスト</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/518400799d.js"></script>
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/item.css">

</head>
<body>
	<header>
		<p>SELECT STORE</p>
		<nav>
			<ul>
				<li><a href="admin_index.html"><i class="fa fa-home"></i> 管理者画面</a></li>
					<li><a href="admin_user_list.html"><i class="fas fa-user"></i> ユーザーリスト</a></li>
					<li><a href="admin_item_regist.html"><i class="fas fa-edit"></i> 商品登録</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<br>
  		<div style="text-align:center">
				<h1><b>商品リクエスト一覧</b></h1>
					<div id="line"></div>
		</div>


			<table class="table">
 				 <thead class="thead-dark">
  					  <tr align="center">
  					  	  <th></th>
   						  <th scope="col">ユーザー名</th>
    					  <th scope="col">商品名</th>
    					  <th scope="col">ブランド名</th>
   					   	  <th scope="col">カテゴリ</th>
   					   	  <th></th>
 					  </tr>
 				 </thead>
 				 <tbody>
   					   <tr align="center">
   					   				<td><input type="checkbox" aria-label="Checkbox for following text input"></td>
   								   	<td scope="row">a</td>
    								<td>CREW NECK SWEATSHIRT</td>
      					 			<td>NIKE</td>
    								<td>トップス</td>
    								 <td><button type="button" class="btn btn-danger">×</button></td>
  					   </tr>
   						<tr align="center">
   									<td><input type="checkbox" aria-label="Checkbox for following text input"></td>
    								<td scope="row">b</td>
     								<td>THE ORIGINAL TWIN TIPPED FRED PERRY SHIRT</td>
						     	 	<td>FRED PERRY</td>
						    	    <td>トップス</td>
						    	     <td><button type="button" class="btn btn-danger">×</button></td>
						</tr>
						<tr align="center">
									<td><input type="checkbox" aria-label="Checkbox for following text input"></td>
						  		    <td scope="row">c</td>
						      		<td>LAYERED SHIRT</td>
						      		<td>Tommy Hilfiger</td>
						     		<td>トップス</td>
						     		 <td><button type="button" class="btn btn-danger">×</button></td>
						</tr>
			</table>
	<br>
	<br>
			<div style="text-align:center;">
					<button type="button" class="btn btn-primary btn-lg">買い物を続ける  <i class="fas fa-shopping-cart"></i></button>
					<button type="button" class="btn btn-secondary btn-lg">次へ</button>
			</div>
</body>
</html>