<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>管理者画面</title>
	<!-- Boostrapにアクセス-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/518400799d.js"></script>
	<link rel="stylesheet" href="css/login.css">
	<link rel="stylesheet" href="css/header.css">
	<link rel="stylesheet" href="css/item.css">

</head>
	<body>
		<header>
			<p>SELECT STORE</p>
			<nav>
				<ul>
					<li><a href="index.html"><i class="fa fa-home"></i> サイトTOP</a></li>
					<li><a href="admin_user_list.html"><i class="fas fa-user"></i> ユーザーリスト</a></li>
					<li><a href="admin_item_request_list.html"><i class="far fa-heart"></i> 商品リクエスト</a></li>
					<li><a href="admin_item_regist.html"><i class="fas fa-edit"></i> 商品登録</a></li>
				</ul>
			</nav>
		</header>
	<br>
	<br>
		<div style = "text-align:center">
			<h1><b>管理者画面</b></h1>
			<div id="line"></div>

	<br>
		<table class="table3" align="center">
					  <tbody>
					    <tr>
						      <th scope="row">商品番号</th>
						      <td><input type = "text" name="item_id" style="width:100px; height:40px;"></td>
						    </tr>
						     <tr>
						      <th scope="row">カテゴリ</th>
						      <td>
						      	<div style= width:200px;>
						      		<select class="custom-select my-1 mr-sm-2" name="category" id="inlineFormCustomSelectPref">
		   										<option selected>-------</option>
		  											<option value="1">トップス</option>
		    										<option value="2">アウター</option>
		   											<option value="3">ボトムス</option>
		   											<option value="4">アクセサリー</option>
		    										<option value="5">バック</option>
		    										<option value="6">シューズ</option>
 									</select>
 								</div>
							  </td>
						    </tr>
						    <tr>
						      <th scope="row">ブランド</th>
						      <td>
									<input type = "text" name="item_brand" style="width:200px; height:40px;">
 							  </td>
						    </tr>
						    <tr>
						      <th scope="row">商品名</th>
						      <td><input type="text" name="item_name" style="width:500px; height:40px;"></td>
						    </tr>
					  </tbody>
		</table>
	<br>

								<button type="submit" class="btn btn-primary my-1">検索</button>
				</div>
	<br>
	<br>

		<table class="table1" align="center">
 				 <thead class="thead-dark">
  					  <tr align="center">
   						  <th scope="col">商品番号</th>
   						  <th scope="col">カテゴリ</th>
   						  <th scope="col">ブランド</th>
    					  <th scope="col">商品名</th>
   					   	  <th scope="col">金額</th>
   					   	  <th scope="col"></th>
 					  </tr>
 				 </thead>
 				 <tbody>
   					   <tr align="center">
   						   		 <td scope="row">1</td>
   						   	     <td>トップス</td>
   						   	     <td>FRED PERRY</td>
    						 	 <td><a href="item_detail.html">CREW NECK SWEATSHIRT</a></td>
    						 	 <td>¥15120</td>
    						 	 <td><button type="button" class="btn btn-success">編集</button></td>
  					   </tr>
   						<tr align="center">
    								<td scope="row">2</td>
    						  		<td>トップス</td>
   						   	 	    <td>FRED PERRY</td>
     								<td><a href="item_detail.html">THE ORIGINAL TWIN TIPPED FRED PERRY SHIRT</a></td>
						    	    <td>¥12,960</td>
						    	    <td><button type="button" class="btn btn-success">編集</button></td>
						</tr>
						<tr align="center">
						   			<td scope="row">3</td>
						   			<td>トップス</td>
   						  	 	    <td>FRED PERRY</td>
						      		<td><a href="item_detail.html">LAYERED SHIRT</a></td>
						     		<td>¥16,200</td>
						     		<td><button type="button" class="btn btn-success">編集</button></td>
						</tr>
				</tbody>
			</table>
	<br>
	<br>

	</body>
</html>